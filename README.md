# Fork an upstream repository and make it your own.

A simple BASH script of [this well-documented GIT process](https://blog.scottlowe.org/2015/01/27/using-fork-branch-git-workflow/), which hopefully saves time and avoids human error. Read it carefully!
