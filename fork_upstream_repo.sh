#!/bin/bash

ORIGINALrepo_URL="https://github.com/jquery/jquery.git"   # https://github.com/jquery/jquery.git
MAINTAINERS_NAME="jquery"   # jquery
UPstream_BRANCH="main" # Always use HEAD!
### The naming convention adds the suffix "_fork" to the GIT repository name, when created using GitLab for example.
NEWrepo_URL="git@xxxxx.yyy:zzzzz/jquery-fork.git"   # https://xxxx.zzz/jquery-fork.git <- Your new GIT repo URL

newBranchFeatureName="InitialAMSbranch"  # Name of the initial branch

### Don't edit anything below this line ###

### Create pseudo-variables START ###
NewORIGINrepo=$MAINTAINERS_NAME"_fork"  # jquery_fork
    echo $NewORIGINrepo  # jquery_fork expected

# The TrackingBRANCH variable defines the 'Upstream Repo + Branch' we'll keep up-to-date
TrackingBRANCH="${MAINTAINERS_NAME}_origin"
    echo $TrackingBRANCH  # jquery_origin expected


# New pseudo-variable, from 'newly ORIGIN repository + original branch name', # jquery_fork/main
# NewORIGINrepoUPSTREAMbranch="old-origin/${UPstream_BRANCH}"
NewORIGINrepoUPSTREAMbranch="old-origin/${UPstream_BRANCH}"
    echo $NewORIGINrepoUPSTREAMbranch  # old-origin/main expected

### Create pseudo-variables END ###

### GIT action START ###

## Making a new Local Clone
git clone $ORIGINALrepo_URL $NewORIGINrepo
cd $NewORIGINrepo  # jquery_fork expected

git checkout $UPstream_BRANCH

git remote rename origin old-origin
# we want to use jquery_fork/origin on our own GIT server
# we now also have jquery_fork/old-origin

# git checkout -b jquery_origin old-origin/main  #  <--Our Tracking branch
git checkout -b $TrackingBRANCH $NewORIGINrepoUPSTREAMbranch  #  <--Our Tracking branch
# git branch --track jquery_origin old-origin/main # <- untested, and unclear

# EXPECTED RESULTS:
#On branch jquery_origin
#Your branch is up to date with 'origin/jquery_origin'.

## Adding a Remote
########### git remote add jquery_fork https://xxxx.zzz/jquery-fork.git <- Your new GIT repo
git remote add origin git@gitlab.com:amstercad/jquery-fork.git
git push -u origin --all
git push -u origin --tags

######################
### GIT action END ###
######################


## Working in a new feature branch
git checkout $TrackingBRANCH  # jquery_origin expected
git pull # pulls HEAD changes from upstream repo

git checkout -b #!/bin/bash

ORIGINALrepo_URL="https://github.com/opdavies/dransible.git"   # https://github.com/jquery/jquery.git
MAINTAINERS_NAME="dransible"   # jquery
UPstream_BRANCH="main" # Always use HEAD!
### The naming convention adds the suffix "_fork" to the GIT repository name, when created using GitLab for example.
NEWrepo_URL="git@gitlab.lan:lee/dransible.git"   # https://xxxx.zzz/jquery-fork.git <- Your new GIT repo URL

newBranchFeatureName="InitialAMSbranch"

### Don't edit anything below this line ###

### Create pseudo-variables START ###
NewORIGINrepo=$MAINTAINERS_NAME"_fork"  # jquery_fork
    echo $NewORIGINrepo  # jquery_fork expected

# The TrackingBRANCH variable defines the 'Upstream Repo + Branch' we'll keep up-to-date
TrackingBRANCH="${MAINTAINERS_NAME}_origin"
    echo $TrackingBRANCH  # jquery_origin expected


# New pseudo-variable, from 'newly ORIGIN repository + original branch name', # jquery_fork/main
# NewORIGINrepoUPSTREAMbranch="old-origin/${UPstream_BRANCH}"
NewORIGINrepoUPSTREAMbranch="old-origin/${UPstream_BRANCH}"
    echo $NewORIGINrepoUPSTREAMbranch  # old-origin/main expected

### Create pseudo-variables END ###

### GIT action START ###

## Making a new Local Clone
git clone $ORIGINALrepo_URL $NewORIGINrepo
cd $NewORIGINrepo  # jquery_fork expected

git checkout $UPstream_BRANCH

git remote rename origin old-origin
# we want to use jquery_fork/origin on our own GIT server
# we now also have jquery_fork/old-origin

# git checkout -b jquery_origin old-origin/main  #  <--Our Tracking branch
git checkout -b $TrackingBRANCH $NewORIGINrepoUPSTREAMbranch  #  <--Our Tracking branch
# git branch --track jquery_origin old-origin/main # <- untested, and unclear

# EXPECTED RESULTS:
#On branch jquery_origin
#Your branch is up to date with 'origin/jquery_origin'.

## Adding a Remote
########### git remote add jquery_fork https://xxxx.zzz/jquery-fork.git <- Your new GIT repo
git remote add origin git@gitlab.com:amstercad/jquery-fork.git
git push -u origin --all
git push -u origin --tags

######################
### GIT action END ###
######################


## Working in a new feature branch
git checkout $TrackingBRANCH  # jquery_origin expected
git pull # pulls HEAD changes from upstream repo

git checkout -b $newBranchFeatureName # Name your new feature branch here and got to work.

 # Name your new feature branch here and got to work.


